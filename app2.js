const http = new XMLHttpRequest();

async function worker() {
    const query = "who was Adolf H.....";
    const payload = {
        prompt: {
            text: `You are ChatOne a Friendly helper who helps by answering to conversational queries, You were developed by Prajjawal Mishra, help your client for the query ${query} in plain text with emojis if needed else plain text only`
        }
    };
    const api_key = "AIzaSyCZd02vQ3KfbNKQOacheS6GEVbdeG-TYjQ"
    const url = `https://generativelanguage.googleapis.com/v1beta2/models/text-bison-001:generateText?key=${api_key}`;

    return new Promise((resolve, reject) => {
        http.open('POST', url, true);
        http.setRequestHeader('Content-Type', 'application/json');
        http.onreadystatechange = function() {
            if (http.readyState === 4 && http.status === 200) {
                const text = JSON.parse(http.responseText);
                resolve(text.candidates[0].output);
            } else if (http.readyState === 4) {
                reject(http.statusText);
            }
        };
        http.send(JSON.stringify(payload));
    });
}

async function main() {
    try {
        const translatedText = await worker();
        console.log(translatedText);
    } catch (error) {
        console.error(error);
    }
}
main()